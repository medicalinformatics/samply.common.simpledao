# Samply Simple DAO

Samply Simple DAO is a *simple* data access object library. It offers simple classes that
simplify the access to a relational database, especially PostgreSQL. Is also offers a well
defined upgrade mechanism for the developer.


## Build

Use maven to build the `war` file:

```
mvn clean package
```
