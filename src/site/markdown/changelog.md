# Changelog

## Version 1.9.1

- added some more debugger output when an exception is thrown

## Version 1.9.0

- the upgrader now uses a two step upgrade, first it executes all sql files, then all java code
- switched to SLF4J
- fixed the logger output of the binder to `TRACE`

## Version 1.8.0

- Open Source
- updated dependencies

## Version 1.7.0

- added the first maven site documentation
- added support for UUIDs
- added missing license headers
